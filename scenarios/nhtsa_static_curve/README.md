## Static Object Encounter Along a Straight Road

**Typical Scenario**:  In this scenario, the ego vehicle travels along a straight line path and encounters a static object present in the middle of the road. The scenario takes place in daylight, and in clear weather, and the posted speed limit is 50 kph.

**Reaction Level Variations**: The lateral distance of the static object to the ego is varied across five different levels ranging from very close to far.