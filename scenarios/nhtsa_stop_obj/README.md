## Left Turn Against Lateral Crossing Dynamic Object

**Typical Scenario**: In this scenario, the ego vehicle comes to a stop at a stop sign at an intersection, and then turns left as a lateral oncoming vehicle also approaches from the left. The scenario takes place in daylight, and in clear weather, and the posted speed limit is 50 kph.

**Interaction Level Variations**: The proximity of the lateral oncoming vehicle to the intersection is varied across five different levels.