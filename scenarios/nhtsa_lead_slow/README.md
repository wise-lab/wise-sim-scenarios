## Following Vehicle Approaching Lead Vehicle Moving at Lower Constant Speed

**Typical Scenario**: In this scenario, a single lead vehicle travels along a straight line path in the same direction as the ego with a speed that is less than or equal to 35 kph. This speed is constant and always lower than the ego's initial speed. The scenario takes place in daylight, and in clear weather conditions, and the posted speed limit is 50 kph.

**Interaction Level Variations**: The speed of the lead vehicle was varied at five equidistant levels ranging between 10 -- 35 kph.