## Running a stop sign

**Typical Scenario**: In this scenario, a single dynamic object travels along a straight line path with a posted speed limit less than or equal to 50 kph and runs a stop sign at the intersection. The scenario takes place in daylight, and in clear weather conditions.

**Interaction Level Variations**: The starting position of the dynamic object is varied at five different levels.