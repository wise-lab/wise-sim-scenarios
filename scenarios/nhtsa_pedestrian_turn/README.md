## Pedestrian Encounter During a Turn

**Typical Scenario**: In this scenario, the ego vehicle turns right at a signaled intersection and encounters a pedestrian. The scenario takes place in daylight, and in clear weather conditions, and the posted speed limit is 50 kph.

**Interaction Level Variations**: The speed of the pedestrian is varied at five different levels between slow to inhumanely fast.