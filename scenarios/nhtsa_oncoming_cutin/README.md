## Oncoming Vehicle Cuts Into The Line of Ego For a Collision

**Typical Scenario**: In this scenario, a single oncoming vehicle travels in the opposite direction as the ego and suddenly cuts into the line of ego for a head-on collision. The scenario takes place in daylight, and in clear weather conditions, and the posted speed limit 50 kph.

**Interaction Level Variations**: The oncoming vehicle cuts-in progressively later for the five different levels making it more difficult for the ego to react.
