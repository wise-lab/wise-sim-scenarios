## Pedestrian Encounter Along a Straight Path

**Typical Scenario**: In this scenario, the ego vehicle travels along a straight line path and encounters a pedestrian running into the middle of the road. The scenario takes place in daylight, and in clear weather conditions, and the posted speed limit is 50 kph.

**Interaction Level Variations**: The speed of the pedestrian is varied at five different levels between slow to inhumanely fast.