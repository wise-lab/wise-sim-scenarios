## Left Turn Across Path of Dynamic Object in Opposite Direction

**Typical Scenario**: In this scenario, the ego vehicle takes a left turn at a non-signaled intersection and cuts across the path of a dynamic object traveling in the opposite direction. The scenario takes place in daylight, and in clear weather, and the posted speed limit is 50 kph.

**Interaction Level Variations**: The proximity of the dynamic object to the intersection is varied across five different levels.