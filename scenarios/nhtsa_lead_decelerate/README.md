## Following Vehicle Approaching a Decelerating Lead Vehicle

**Typical Scenario**: In this scenario, a single lead vehicle travels along a straight line path in the same direction as the ego and suddenly decelerates while the ego is following. The scenario takes place in daylight, and in clear weather conditions, and the posted speed limit 50 kph.

**Interaction Level Variations**: The amount of deceleration was varied at five different levels ranging from immediate stop to a slow stop.