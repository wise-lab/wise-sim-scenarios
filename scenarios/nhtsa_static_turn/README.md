## Static Object Encounter During a Turn

**Typical Scenario**: In this scenario, the ego vehicle turns right at an intersection and encounters a static object present in the parking lane or on the shoulder of the road. The scenario takes place in daylight, and in clear weather, and the posted speed limit is 50 kph.

**Reaction Level Variations**: The lateral distance of the static object to the ego is varied across five different levels ranging from very close to far.