## Overtake with Dynamic Object in Opposite Direction

**Typical Scenario**: In this scenario, the ego vehicle travels along a straight road and attempts to pass another vehicle. At the same time, it encounters a third vehicle traveling in the opposite direction. The scenario takes place in daylight, and in clear weather, and the posted speed limit is 50 kph.

**Interaction Level Variations**: The time to collision with the vehicle in the opposite direction is varied across five different levels.