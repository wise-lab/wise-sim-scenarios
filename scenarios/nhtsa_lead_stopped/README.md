## Following Vehicle Approaching a Stopped Lead Vehicle

**Typical Scenario**: In this scenario, the ego vehicle approaches a stopped lead vehicle positioned in the middle of the road. The scenario takes place in daylight, and in clear weather conditions, and the posted speed limit is 50 kph.

**Interaction Level Variations**: The position of the lead vehicle is varied at five different levels to be closer or further away from an intersection.