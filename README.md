<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a href="https://git.uwaterloo.ca/wise-lab/wise-sim-scenarios/">WISE Sim Example Scenarios</a></span>
by <a xmlns:cc="http://creativecommons.org/ns#" href="https://uwaterloo.ca/waterloo-intelligent-systems-engineering-lab/" property="cc:attributionName" rel="cc:attributionURL">Waterloo Intelligent Systems Engineering (WISE) Lab </a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

# WISE Sim Example Scenarios

Example simulation scenarios for [WISE Sim](https://uwaterloo.ca/waterloo-intelligent-systems-engineering-lab/projects/wise-sim).

## License

All content in this repository is provided under
Creative Commons License Attribution 4.0 International.

All content in the binary ("object") form, including

* WISE Sim,
* WISE ADS,

are provided under the MIT license.

# Installation

Executing the scenarios requires WISE Automated Driving System (ADS)
and the WISE Sim simulator.

### Hardware Requirements

* 64 bit machine with ~3 Gb of free disk space
* Graphics accelerator (tested only with NVidia GPUs)

### Software Requirements

* Ubuntu 20.04 Focal + Robot Operating System (ROS) Noetic
* All dependencies including ROS Noetic are installed automatically

In order to update the software to the latest versions simply repeat the installation procedure.

## Installing WISE ADS

To download the latest version and install the WISE ADS software to
`/opt/ros/wise-ads`, execute

```
$ bash scripts/wise_ads_install.bash
```

Re-open the terminal to source the updated environment.

To install without downloading and use the previously downloaded archive, execute

```
$ bash scripts/wise_ads_install.bash --no-download
```

In general,

```
$ bash scripts/wise_ads_install.bash -h
Usage

   bash wise_ads_install.bash [-n|--no-download] [-h|--help]
     -n, --no-download      use the already downloaded wise-ads-focal-noetic.tar.xz
     -h, --help             display this usage information and exit
```

### Upgrading the ADS

Simply, re-run the installation script, which will download the latest version. 

## Installing or upgrading WISE Sim

To install the WISE Sim to `/opt/anm_unreal_sim` (on Bionic) or `/opt/wise-sim` (on Focal) and the user environment, execute

```
$ bash scripts/wise_sim_install.bash
```

The script will also automatically upgrade the simulator if a newer version
is available for download.

Alternatively, the two steps can be performed separately.
To only install WISE Sim to `/opt/`, execute

```
$ bash scripts/install_wise_sim.bash
```

To install the environment for the user, execute

```
$ bash scripts/install_wise_sim_env.bash
```

The alternative procedure is useful for creating Docker images.

Re-open the terminal to source the updated environment.

# Usage

**[Running Scenarios](docs/Running_scenarios.md)**

**Running Benchmarks (docs/Running_benchmarks.md)** (NOTE: this file is symlinked from the package `anm_sim_platform` during WISE Sim environment installation)

**[Creating and Modifying Scenarios](docs/Creating_scenarios.md)**

**[Creating and Modifying Scenario Suites](docs/Creating_scenario_suites.md)**

**Vehicle Model (docs/Vehicle_model.md)** (NOTE: this file is symlinked from the package `dbw_mkz_moose` during WISE Sim environment installation)
