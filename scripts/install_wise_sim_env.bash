#!/bin/bash
set -e
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
REPO_DIR="$( dirname "$SCRIPT_DIR" )"
ROS_DISTRO=$(rosversion -sd)

# ensure rospack is available
if [[ -z `which rospack` ]]
then
    source "/opt/ros/$ROS_DISTRO/setup.bash"
fi
if [[ "$ROS_DISTRO" == "noetic" ]]; then
    SIM_NAME=wise-sim
    SIM_PLATFORM=wise_ads_sim_platform
    ROS_SOURCE="wise-sim/local_setup.bash"
elif [[ "$ROS_DISTRO" == "melodic" ]]; then
    SIM_NAME=anm_unreal_simulator
    SIM_PLATFORM="anm_sim_platform"
    ROS_SOURCE="ros_unreal/setup.bash --extend"
else
    echo "Unsupported ROS version: only noetic and melodic are supported."
    exit 1
fi
set +e
set +o pipefail
# ensure wise-ads packages are available
if (rospack find hdmap_server 2>&1 | grep -q "not found")
then
   source /opt/ros/wise-ads/local_setup.bash
fi
set -e
set -o pipefail

# Allow overriding INSTALL_DIR, default to /
: ${INSTALL_DIR:=""}

# symlink the simulator
ln -sfn "${INSTALL_DIR}/opt/${SIM_NAME}" "$REPO_DIR"
echo "Created a symlink to `readlink $REPO_DIR/${SIM_NAME}`"

# symlink the geoscenarioserver
ln -sfn "${INSTALL_DIR}/opt/geoscenarioserver" "$REPO_DIR"
echo "Created a symlink to `readlink $REPO_DIR/geoscenarioserver`"

# symlink_map_files

MAP_SERVER_CONFIG_DIR="$(rospack find hdmap_server)/config"
if [[ -d "$REPO_DIR/maps/avin/" ]]; then
    ln -sfn "$MAP_SERVER_CONFIG_DIR/lanelet2_maps/avin.osm" "$REPO_DIR/maps/avin/"
fi
if [[ -d "$REPO_DIR/maps/bathurst_drive/" ]]; then
    ln -sfn "$MAP_SERVER_CONFIG_DIR/bathurst.osm" "$REPO_DIR/maps/bathurst_drive/"
fi
if [[ -d "$REPO_DIR/maps/colby_drive/" ]]; then
    ln -sfn "$MAP_SERVER_CONFIG_DIR/colby.osm" "$REPO_DIR/maps/colby_drive/"
fi
if [[ -d "$REPO_DIR/maps/ring_road/" ]]; then
    ln -sfn "$MAP_SERVER_CONFIG_DIR/lanelet2_maps/avin.osm" "$REPO_DIR/maps/ring_road/ring_road.osm"
fi

echo "Symlinked OSM maps from $MAP_SERVER_CONFIG_DIR/"

# ensure wise-sim packages are available
set +e
set +o pipefail
# ensure ${SIM_PLATFORM} package is available
# respect the version the user may have already sourced
if (rospack find ${SIM_PLATFORM} 2>&1 | grep -q "not found"); then
    source "/opt/ros/${ROS_SOURCE}"
fi

set -e
set -o pipefail

# symlink ${SIM_PLATFORM} folders
ANM_SIM_PLATFORM_DIR="$(rospack find ${SIM_PLATFORM})"
ln -sfn "$ANM_SIM_PLATFORM_DIR/docs" "$REPO_DIR"
ln -sfn "$ANM_SIM_PLATFORM_DIR/rviz" "$REPO_DIR"
ln -sfn "$ANM_SIM_PLATFORM_DIR/josm" "$REPO_DIR"

echo "Symlinked folders docs/ rviz/ and josm/ from $ANM_SIM_PLATFORM_DIR/"

# set up simulator ROS packages
if ! grep -q "source /opt/ros/${ROS_SOURCE}" "$HOME/.bashrc"; then
    # append the correct sourcing command with --extend
    echo "source /opt/ros/${ROS_SOURCE}" >> "$HOME/.bashrc"
    echo "Set up WISE Sim ROS packages"
fi

SIM_PLATFORM_SCRIPTS_DIR="$(rospack find ${SIM_PLATFORM})/scripts"
if ! grep -q "source $SIM_PLATFORM_SCRIPTS_DIR/short_launch.bash"  ~/.bashrc
then
    echo "source $SIM_PLATFORM_SCRIPTS_DIR/short_launch.bash" >> ~/.bashrc
    echo "Set up short launch autocomplete"
fi

echo "WISE Sim user environment setup successfully."
echo ""
