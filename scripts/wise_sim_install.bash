#!/bin/bash
set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Installing the latest version of WISE Sim..."
bash "${SCRIPT_DIR}/install_wise_sim.bash"
bash "${SCRIPT_DIR}/install_wise_sim_env.bash"

echo "WISE Sim installation completed!"
echo "Reopen the terminal to get the new environment"
echo ""
