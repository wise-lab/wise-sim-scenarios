#!/bin/bash
set -e

# ensure we have wget and unzip installed
sudo apt-get install -qq -y wget

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
REPO_DIR="$( dirname $SCRIPT_DIR )"
DOWNLOADS_DIR="$REPO_DIR/downloads"
UBUNTU_CODENAME=$(lsb_release -sc)
# We assume users do not have ROS installed
if [[ "${UBUNTU_CODENAME}" == "focal" ]]; then
    ROS_DISTRO="noetic"
else
    echo "Unsupported Ubuntu version detected (${UBUNTU_CODENAME})."
    echo "Only Ubuntu 20.04 Focal and ROS Noetic are supported."
    exit 1
fi

ARG_NO_DOWNLOAD=
ARG_QUICK=
for arg in "$@"; do
    case $arg in
        -n | --no-download)
            ARG_NO_DOWNLOAD="true";;
        -q | --quick)
            ARG_QUICK="true";;
        -h | --help)
            echo "Usage"
            echo ""
            echo "   bash wise_ads_install.bash [-n|--no-download] [-h|--help]"
            echo "     -n, --no-download      use the already downloaded wise-ads-${UBUNTU_CODENAME}-${ROS_DISTRO}.tar.xz"
            echo "     -q, --quick            skip installing the dependencies"
            echo "     -h, --help             display this usage information and exit"
            echo ""
            exit;;
    esac
done

SERVER_ADDR="wiselab.uwaterloo.ca"
# Either wise-ads-bionic-melodic.zip or wise-ads-focal-noetic.zip
WISE_ADS_TAR_XZ="wise-ads-${UBUNTU_CODENAME}-${ROS_DISTRO}.tar.xz"

cd "$DOWNLOADS_DIR"
if [[ -f "$DOWNLOADS_DIR/$WISE_ADS_TAR_XZ" && "$ARG_NO_DOWNLOAD" == "true" ]]; then
    echo "Using previously downloaded '$DOWNLOADS_DIR/$WISE_ADS_TAR_XZ'..."
    echo "Remove it if you wish to download a new one"
else
    echo "Downloading WISE ADS ($WISE_ADS_TAR_XZ)..."
    if [[ -f "$DOWNLOADS_DIR/$WISE_ADS_TAR_XZ" ]]; then
        rm "$DOWNLOADS_DIR/$WISE_ADS_TAR_XZ"
    fi
    wget "$SERVER_ADDR/wise-sim/$WISE_ADS_TAR_XZ"
fi

# setup road demo
echo "Unpacking $WISE_ADS_TAR_XZ to /opt/ros/wise-ads..."
tar xfJ "$WISE_ADS_TAR_XZ" wise-ads
if [ -d /opt/ros/ ]; then
    sudo rm -rf /opt/ros/wise-ads
else
    sudo mkdir -p /opt/ros/
fi
sudo mv wise-ads /opt/ros/

if [[ $ARG_QUICK == "true" ]]; then
    echo "Skipping the installation of WISE ADS dependencies"
    cd /opt/ros/wise-ads/behavior_planner/share/behavior_planner/rule-engine/
    source "$HOME/.nvm/nvm.sh"  # ensure npm is available
    npm install
else
    echo "Installing WISE ADS dependencies"
    cd /opt/ros/wise-ads/scripts/
    bash prepare_target.bash

    # set up WISE ADS ROS packages
    if ! grep -q "source /opt/ros/wise-ads/local_setup.bash" "$HOME/.bashrc"; then
        echo "source /opt/ros/wise-ads/local_setup.bash" >> "$HOME/.bashrc"
        echo "Added sourcing of WISE ADS ROS packages to .bashrc"
    fi
fi

if [[ $(which notify-send) ]]; then
    notify-send --urgency=low -i terminal "WISE ADS installation completed!"
fi
echo "WISE ADS installation completed!"
echo "Reopen the terminal to source the new environment."
echo ""
