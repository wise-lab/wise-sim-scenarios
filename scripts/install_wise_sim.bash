#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
REPO_DIR="$( dirname "$SCRIPT_DIR" )"
UBUNTU_CODENAME=$(lsb_release -sc)
ROS_DISTRO=$(rosversion -sd)
if [[ $? -gt 0 ]]; then
    echo "Cannot determine ROS version: ensure ROS is installed."
    exit 1
elif [[ "${ROS_DISTRO}" == "<unknown>" ]]; then
    echo "Cannot determine ROS version: ensure '/opt/ros/<ROS version>/setup.bash' is sourced."
    exit 1
fi
if [[ "$ROS_DISTRO" == "noetic" ]]; then
    SIM_NAME=wise-sim
    SIMULATOR_ARCHIVE="${SIM_NAME}-${UBUNTU_CODENAME}-${ROS_DISTRO}.tar.xz"
elif [[ "$ROS_DISTRO" == "melodic" ]]; then
    SIM_NAME=anm_unreal_simulator
    SIMULATOR_ARCHIVE="${SIM_NAME}-${UBUNTU_CODENAME}-${ROS_DISTRO}.zip"
else
    echo "Unsupported ROS version: only noetic and melodic are supported."
    exit 1
fi
SERVER_ADDR="wiselab.uwaterloo.ca"

# Allow overriding INSTALL_DIR, default to /
: ${INSTALL_DIR:=""}

function prepare_target() {
    bash "${INSTALL_DIR}/opt/${SIM_NAME}/prepare_target.bash"
}

function server_ping_check() {
    # ensure ping is available
    sudo apt-get install -qqq iputils-ping
    # Check if server can be pinged
    if ping -c 1 -W 1 "$SERVER_ADDR" &> /dev/null; then
        return 0
    else
        echo "ERROR: The server $SERVER_ADDR can not be reached."
        echo "Please check your internet connection."
        return 1
    fi
}

function download_wise_sim() {
    echo "Downloading the latest simulator version ..."
    cd "${REPO_DIR}/downloads"
    wget --no-verbose "${SERVER_ADDR}/wise-sim/$SIMULATOR_ARCHIVE"
    return $?
}

function check_version_download_and_install() {
    # no arguments
    VERSION_FILE_SUFFIX="opt/${SIM_NAME}/version.txt"
    REMOTE_VERSION=$(curl -s "https://${SERVER_ADDR}/wise-sim/${SIM_NAME}-version.txt")
    # Check the version, this function will return if already up to date
    if [[ -f "${INSTALL_DIR}/${VERSION_FILE_SUFFIX}" ]]; then
        LOCAL_VERSION=$(cat "${INSTALL_DIR}/${VERSION_FILE_SUFFIX}")
        if [[ "$LOCAL_VERSION" == "$REMOTE_VERSION" ]]; then
            echo "You already have the latest simulator version installed:"
            echo ""
            echo "$LOCAL_VERSION"
            echo ""
            return 0
        else
            echo "The installed simulator is outdated. Upgrading..."
            echo ""
            echo "Local version:"
            echo ""
            echo "$LOCAL_VERSION"
            echo ""
            echo "New version:"
            echo ""
            echo "$REMOTE_VERSION"
            echo ""
        fi
    else
        echo "WISE Sim not installed. Installing..."
        echo ""
        echo "$REMOTE_VERSION"
        echo ""
    fi

    # No installed version or an outdated version, see if we have a cached download
    mkdir -p "${REPO_DIR}/downloads"
    if [[ -f "${REPO_DIR}/downloads/${SIMULATOR_ARCHIVE}" ]]; then
        if [[ "$ROS_DISTRO" == "noetic" ]]; then
            CACHED_VERSION=$(tar xfJO "${REPO_DIR}/downloads/${SIMULATOR_ARCHIVE}" --wildcards "opt/wise-sim-*/version.txt")
        elif [[ "$ROS_DISTRO" == "melodic" ]]; then
            CACHED_VERSION=$(unzip -p "${REPO_DIR}/downloads/${SIMULATOR_ARCHIVE}" "opt/${SIM_NAME}_*/version.txt")
        fi
        if [[ "$CACHED_VERSION" == "$REMOTE_VERSION" ]]; then
            echo "The cached download is the latest simulator version."
            echo ""
            # proceed with the installation
        else
            echo "The cached version is outdated, removing."
            rm "${REPO_DIR}/downloads/${SIMULATOR_ARCHIVE}"
            download_wise_sim
        fi
    else
        # no cached version
        download_wise_sim
    fi
    # Install from the archive
    echo "Unpacking the simulator to ${INSTALL_DIR}/opt/${SIM_NAME}/ and ROS packages to ${INSTALL_DIR}/opt/ros/ ..."
    if [[ "$ROS_DISTRO" == "noetic" ]]; then
        cd "${INSTALL_DIR}/"
        sudo tar xfJ "${REPO_DIR}/downloads/${SIMULATOR_ARCHIVE}"
    elif [[ "$ROS_DISTRO" == "melodic" ]]; then
        sudo unzip -qq -o "${REPO_DIR}/downloads/${SIMULATOR_ARCHIVE}" -d "${INSTALL_DIR}/"
    fi
    echo ""
    prepare_target
    echo "Successfully installed WISE Sim"
}

# Call the function if the script is executed.
# Do nothing when sourced.
if [[ ${BASH_SOURCE[0]} = "${0}" ]]; then
    set -e
    server_ping_check
    check_version_download_and_install
fi
