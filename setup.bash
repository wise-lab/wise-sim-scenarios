SUITE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [[ -z $WISE_SIM_SCENARIO_SUITES ]]; then
    export WISE_SIM_SCENARIO_SUITES=$SUITE_DIR
else
    export WISE_SIM_SCENARIO_SUITES="${WISE_SIM_SCENARIO_SUITES}:$SUITE_DIR"
fi

if [[ -n $1 && $1 == --install ]]; then
    script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    echo "source $script_dir/setup.bash" >> $HOME/.bashrc
fi